<?php
/**
 * Not Existent Database Exception
 * 
 * @author Savio Resende <savio@savioresende.com.br>
 */

namespace Models\Exceptions;

class NotExistentDatabaseException extends \Exception
{
	
}